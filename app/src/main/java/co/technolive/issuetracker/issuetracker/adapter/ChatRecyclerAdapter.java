package co.technolive.issuetracker.issuetracker.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.technolive.issuetracker.issuetracker.R;
import co.technolive.issuetracker.issuetracker.model.ChatMessageModel;


import static android.content.ContentValues.TAG;

/**
 * Created by ahmed on 11-02-18.
 */

public class ChatRecyclerAdapter
        extends RecyclerView.Adapter<ChatRecyclerAdapter.ViewHolder> {

    private ArrayList<ChatMessageModel> listItems;
    private Context context;


    public ChatRecyclerAdapter(ArrayList<ChatMessageModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_layout_items, parent, false);
        return new ViewHolder(view);

    }

    public void refreshMessages(ArrayList<ChatMessageModel> list) {
        this.listItems.clear();
        this.listItems.addAll(list);
        this.notifyDataSetChanged();
    }


    public void addSingleMessage(ChatMessageModel model) {
        this.listItems.add(model);
        this.notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.v(TAG, "content of array: " + listItems.get(position));
        ChatMessageModel model = listItems.get(position);
        if (model.isSelfMessage()) {
            holder.senderText.setText(model.getMessage());
            holder.senderRl.setVisibility(View.VISIBLE);
            holder.receiverRl.setVisibility(View.GONE);
        } else {
            holder.receiverRl.setVisibility(View.VISIBLE);
            holder.senderRl.setVisibility(View.GONE);
            holder.receiverText.setText(model.getMessage());
        }
//        holder.receiverNameTextView.setText(model.getSenderName());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout senderRl, receiverRl;
        TextView senderText, receiverText;


        public ViewHolder(View itemView) {
            super(itemView);
            senderRl = (RelativeLayout) itemView.findViewById(R.id.sender_rl);
            receiverRl = (RelativeLayout) itemView.findViewById(R.id.receiver_rl);
            senderText = (TextView) itemView.findViewById(R.id.chat_sender_layout_text);
            receiverText = (TextView) itemView.findViewById(R.id.chat_receiver_layout_text);
//            receiverNameTextView = (TextView) itemView.findViewById(R.id.chat_receiver_name);
        }
    }

}
