package co.technolive.issuetracker.issuetracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.technolive.issuetracker.issuetracker.R;

/**
 * Created by ahmed on 11-02-18.
 */

public class MessageFragmentAdapter extends RecyclerView.Adapter<MessageFragmentAdapter.ViewHolder> {
    private ArrayList<String> listItems;
    private Context context;


    public MessageFragmentAdapter(ArrayList<String> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_fragment_items, parent, false);



        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MessageFragmentAdapter.ViewHolder holder, final int position) {
        holder.profileName.setText(listItems.get(position));
        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent("chosen chat");
                i.putExtra("position",position);
                LocalBroadcastManager.getInstance(context).sendBroadcast(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView profileName;
        public TextView chatTextHead;
        RelativeLayout rl;

        public ViewHolder(View itemView) {
            super(itemView);
            rl=(RelativeLayout) itemView.findViewById(R.id.message_fragment_relative_layout);
            profileName = (TextView) itemView.findViewById(R.id.profile_name);
            chatTextHead = (TextView) itemView.findViewById(R.id.chat_text_head);

        }
    }
}
