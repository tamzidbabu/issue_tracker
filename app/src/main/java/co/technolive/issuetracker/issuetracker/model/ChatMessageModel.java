package co.technolive.issuetracker.issuetracker.model;

import java.util.Date;

/**
 * Created by munta on 20-02-2018.
 */
public class ChatMessageModel{
    private String message;
    private String senderName;
    private Date createdAt;
    private boolean isSelfMessage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public boolean isSelfMessage() {
        return isSelfMessage;
    }

    public void setSelfMessage(boolean selfMessage) {
        isSelfMessage = selfMessage;
    }
}