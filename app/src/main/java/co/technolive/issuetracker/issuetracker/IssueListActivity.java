package co.technolive.issuetracker.issuetracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class IssueListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_list);

        String nameTitle = getIntent().getStringExtra("name");

        TextView tv = (TextView) findViewById(R.id.title);
        tv.setText(nameTitle+"");
        findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IssueListActivity.this,IssueDetailsActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IssueListActivity.this,IssueDetailsActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IssueListActivity.this,IssueDetailsActivity.class);
                startActivity(intent);
            }
        });
    }
}
