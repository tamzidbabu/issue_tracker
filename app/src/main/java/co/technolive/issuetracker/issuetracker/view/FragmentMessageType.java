package co.technolive.issuetracker.issuetracker.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import co.technolive.issuetracker.issuetracker.PagerFragment;
import co.technolive.issuetracker.issuetracker.R;
import co.technolive.issuetracker.issuetracker.adapter.ChatRecyclerAdapter;
import co.technolive.issuetracker.issuetracker.adapter.MessageFragmentAdapter;
import co.technolive.issuetracker.issuetracker.model.ChatMessageModel;

public class FragmentMessageType extends Fragment {
    // Store instance variables
    private String title;
    private int page;
    ChatRecyclerAdapter adapter;
    @BindView(R.id.chat_layout_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sender_edit_text)
    EditText chatText;
    public static String user = "me";//subject to change
    ArrayList<String> sentChatText, receivedChatText;
    @BindView(R.id.chat_text_send_button)
    ImageView sendButton;
    int incomingUserId;
    String incomingCommentId;

    String token;
    int questionId;
    int loggedInUserId;

    // newInstance constructor for creating fragment with arguments
    public static PagerFragment newInstance(int page, String title) {
        PagerFragment fragmentFirst = new PagerFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_type, container, false);
        initRecyclerView(view);


        sendButton = (ImageView) view.findViewById(R.id.chat_text_send_button);
        chatText = (EditText) view.findViewById(R.id.sender_edit_text);



        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String message = chatText.getText().toString();
                if (message.equals("")) {
                    chatText.setError("Please Write a message...!");
                    return;
                }
                if (adapter!= null) {
                    ChatMessageModel msgModel = new ChatMessageModel();
                    msgModel.setMessage(chatText.getText().toString());
                    msgModel.setSelfMessage(false);
                    msgModel.setCreatedAt(new Date());
                    adapter.addSingleMessage(msgModel);
                    recyclerView.scrollToPosition(adapter.getItemCount());
                }

            }
        });

        return view;
    }
    void initRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.chat_layout_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new ChatRecyclerAdapter(new ArrayList<ChatMessageModel>(), getActivity());
        recyclerView.setAdapter(adapter);
    }

}