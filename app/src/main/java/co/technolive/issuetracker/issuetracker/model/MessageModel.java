package co.technolive.issuetracker.issuetracker.model;

/**
 * Created by tamzid on 2/18/18.
 */

public class MessageModel {
    private int id;
    private boolean is_admin_msg;
    private String text;
    private String created_at;
    private int question;
    private String questionText;
    private int user;
    private String userName;

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIs_admin_msg() {
        return is_admin_msg;
    }

    public void setIs_admin_msg(boolean is_admin_msg) {
        this.is_admin_msg = is_admin_msg;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
