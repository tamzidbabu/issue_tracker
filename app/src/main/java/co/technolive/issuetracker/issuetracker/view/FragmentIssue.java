package co.technolive.issuetracker.issuetracker.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import co.technolive.issuetracker.issuetracker.IssueListActivity;
import co.technolive.issuetracker.issuetracker.NewIssueActivity;
import co.technolive.issuetracker.issuetracker.R;


public class FragmentIssue extends Fragment {
    // Store instance variables
    private String title;
    private int page;

    // newInstance constructor for creating fragment with arguments
    public static FragmentIssue newInstance(int page, String title) {
        FragmentIssue fragmentFirst = new FragmentIssue();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issues, container, false);

        ImageView add_user =  (ImageView) view.findViewById(R.id.add_user);
        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),NewIssueActivity.class);

                startActivity(intent);
            }
        });

        view.findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),IssueListActivity.class);
                intent.putExtra("name","Urgent");
                startActivity(intent);
            }
        });


        view.findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),IssueListActivity.class);
                intent.putExtra("name","Pending");
                startActivity(intent);
            }
        });


        view.findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),IssueListActivity.class);
                intent.putExtra("name","Current");

                startActivity(intent);
            }
        });
        view.findViewById(R.id.four).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),IssueListActivity.class);
                intent.putExtra("name","Complete");

                startActivity(intent);
            }
        });

        view.findViewById(R.id.five).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),IssueListActivity.class);
                intent.putExtra("name","Rejected");

                startActivity(intent);
            }
        });




        return view;
    }
}