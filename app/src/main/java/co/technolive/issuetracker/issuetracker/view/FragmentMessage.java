package co.technolive.issuetracker.issuetracker.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.technolive.issuetracker.issuetracker.ChatActivity;
import co.technolive.issuetracker.issuetracker.IssueDetailsActivity;
import co.technolive.issuetracker.issuetracker.IssueListActivity;
import co.technolive.issuetracker.issuetracker.R;

public class FragmentMessage extends Fragment {
    // Store instance variables
    private String title;
    private int page;

    // newInstance constructor for creating fragment with arguments
    public static FragmentMessage newInstance(int page, String title) {
        FragmentMessage fragmentFirst = new FragmentMessage();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);


        view.findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.one1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.one2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.one3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.one4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.one5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });



        return view;
    }
}