package co.technolive.issuetracker.issuetracker.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import co.technolive.issuetracker.issuetracker.view.FragmentFeature;
import co.technolive.issuetracker.issuetracker.view.FragmentIssue;
import co.technolive.issuetracker.issuetracker.view.FragmentMessage;
import co.technolive.issuetracker.issuetracker.view.FragmentPeople;
import co.technolive.issuetracker.issuetracker.PagerFragment;

public class AdapterPager extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 4;

    public AdapterPager(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return FragmentIssue.newInstance(0, "Page # 1");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return FragmentMessage.newInstance(1, "Page # 2");
            case 2: // Fragment # 1 - This will show SecondFragment
                return FragmentPeople.newInstance(2, "Page # 3");
            case 3: // Fragment # 1 - This will show SecondFragment
                return FragmentFeature.newInstance(3, "Page # 4");
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}