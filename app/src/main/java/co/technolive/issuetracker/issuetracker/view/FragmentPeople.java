package co.technolive.issuetracker.issuetracker.view;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.technolive.issuetracker.issuetracker.R;

public class FragmentPeople  extends Fragment {
    private static final String FRAGMENT_TAG = "FragmentPeople" ;
    // Store instance variables
    private String title;
    private int page;

    // newInstance constructor for creating fragment with arguments
    public static FragmentPeople newInstance(int page, String title) {
        FragmentPeople fragmentFirst = new FragmentPeople();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people_layout, container, false);
//        TextView tvLabel = (TextView) view.findViewById(R.id.tvLabel);
//        tvLabel.setText(page + " -- " + title);
        final TextView people_tv = (TextView) view.findViewById(R.id.people_tv);
        people_tv.setBackgroundColor(getResources().getColor(R.color.gray));
        people_tv.setTextColor(getResources().getColor(R.color.white));
        createFrag(R.id.frag_container,new FragmentPeoplePublicChannels());


        final TextView your_channel_tv = (TextView) view.findViewById(R.id.your_channel_tv);
        final TextView contacts_tv = (TextView) view.findViewById(R.id.contacts_tv);


        people_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                people_tv.setBackgroundColor(getResources().getColor(R.color.gray));
                people_tv.setTextColor(getResources().getColor(R.color.white));

                your_channel_tv.setBackgroundColor(getResources().getColor(R.color.white));
                your_channel_tv.setTextColor(getResources().getColor(R.color.gray));

                contacts_tv.setBackgroundColor(getResources().getColor(R.color.white));
                contacts_tv.setTextColor(getResources().getColor(R.color.gray));


                replaceFrag(R.id.frag_container,new FragmentPeoplePublicChannels());
            }
        });


        your_channel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                people_tv.setBackgroundColor(getResources().getColor(R.color.white));
                people_tv.setTextColor(getResources().getColor(R.color.gray));

                your_channel_tv.setBackgroundColor(getResources().getColor(R.color.gray));
                your_channel_tv.setTextColor(getResources().getColor(R.color.white));

                contacts_tv.setBackgroundColor(getResources().getColor(R.color.white));
                contacts_tv.setTextColor(getResources().getColor(R.color.gray));


                replaceFrag(R.id.frag_container,new FragmentPeopleYourChannels());
            }
        });


        contacts_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                people_tv.setBackgroundColor(getResources().getColor(R.color.white));
                people_tv.setTextColor(getResources().getColor(R.color.gray));

                your_channel_tv.setBackgroundColor(getResources().getColor(R.color.white));
                your_channel_tv.setTextColor(getResources().getColor(R.color.gray));

                contacts_tv.setBackgroundColor(getResources().getColor(R.color.gray));
                contacts_tv.setTextColor(getResources().getColor(R.color.white));


                replaceFrag(R.id.frag_container,new FragmentPeopleContacts());


            }
        });


        return view;
    }

    private void createFrag(@IdRes int containerViewId ,@NonNull Fragment fragment){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        fm.beginTransaction();
        Bundle arguments = new Bundle();
        arguments.putBoolean("shouldYouCreateAChildFragment", false);
        fragment.setArguments(arguments);
        ft.add(containerViewId, fragment);
        ft.commit();
    }
    private void replaceFrag(@IdRes int containerViewId ,@NonNull Fragment fragment){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        fm.beginTransaction();
        ft.replace(containerViewId, fragment);
//        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        ft.addToBackStack(null);
        ft.commit();
    }


}